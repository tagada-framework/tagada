FROM --platform=linux/amd64 ubuntu

RUN apt update && \
    apt install -y \
        git \
        vim nano \
        make \
        gcc g++ \
        tar gzip zip unzip build-essential \
        wget curl \ 
        python3 \
        pkg-config libssl-dev \
        graphviz \
        gnupg2

RUN useradd --user-group --system --create-home --no-log-init tagada
SHELL [ "/bin/bash", "-c" ]

# Installing RVM
RUN gpg --keyserver keyserver.ubuntu.com --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB \
    && curl -sSL https://get.rvm.io | bash -s stable \
    && usermod -a -G rvm tagada

# Installing RUBY
ARG RUBY_VERSION=3.3.1
RUN source /usr/local/rvm/scripts/rvm && rvm install "${RUBY_VERSION}"
ENV PATH="/usr/local/rvm/rubies/ruby-${RUBY_VERSION}/bin:${PATH}"

# Installing MiniZinc
ARG MINIZINC_VERSION=2.8.3
WORKDIR /opt/Minizinc-${MINIZINC_VERSION}/

RUN wget https://github.com/MiniZinc/MiniZincIDE/releases/download/${MINIZINC_VERSION}/MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64.tgz \
    && tar -zxvf MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64.tgz \
    && rm MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64.tgz

ENV PATH="/opt/Minizinc-${MINIZINC_VERSION}/MiniZincIDE-${MINIZINC_VERSION}-bundle-linux-x86_64/bin:${PATH}"

# Installing Picat
ARG PICAT_VERSION=364
WORKDIR /opt/Picat-${PICAT_VERSION}
RUN wget http://picat-lang.org/download/picat${PICAT_VERSION}_linux64.tar.gz \
    && tar -zxvf picat${PICAT_VERSION}_linux64.tar.gz \
    && rm picat${PICAT_VERSION}_linux64.tar.gz \
    && cd /opt/Picat-${PICAT_VERSION}/Picat/lib \
    && wget http://picat-lang.org/flatzinc/fzn_tokenizer.pi \
    && wget http://picat-lang.org/flatzinc/fzn_parser.pi \
    && wget http://picat-lang.org/flatzinc/fzn_picat_sat.pi \
    && sed -i "s/    writeln(';'),/    println(';'),/" fzn_picat_sat.pi

ENV PATH="/opt/Picat-${PICAT_VERSION}/Picat:${PATH}"
ENV FZN_PICAT_SAT="/opt/Picat-${PICAT_VERSION}/Picat/lib/fzn_picat_sat.pi"

# Installing OrTools
ARG ORTOOLS_SHORT_VERSION=9.9
ARG ORTOOLS_LONG_VERSION=9.9.3963
WORKDIR /opt/OrTools-${ORTOOLS_SHORT_VERSION}
RUN wget https://github.com/google/or-tools/releases/download/v${ORTOOLS_SHORT_VERSION}/or-tools_amd64_ubuntu-22.04_cpp_v${ORTOOLS_LONG_VERSION}.tar.gz \
    && tar -zxvf or-tools_amd64_ubuntu-22.04_cpp_v${ORTOOLS_LONG_VERSION}.tar.gz \
    && rm or-tools_amd64_ubuntu-22.04_cpp_v${ORTOOLS_LONG_VERSION}.tar.gz

ENV PATH="/opt/OrTools-${ORTOOLS_SHORT_VERSION}/or-tools_x86_64_Ubuntu-22.04_cpp_v${ORTOOLS_LONG_VERSION}/bin:${PATH}"
ENV FZN_ORTOOLS="/opt/OrTools-${ORTOOLS_SHORT_VERSION}/or-tools_x86_64_Ubuntu-22.04_cpp_v${ORTOOLS_LONG_VERSION}/bin/fzn-cp-sat"

WORKDIR /opt
RUN git clone https://github.com/classabbyamp/espresso-logic.git \
  && cd espresso-logic/espresso-src \
  && make

ENV PATH="/opt/espresso-logic/bin:${PATH}"
ENV ESPRESSO_AB="/opt/espresso-logic/bin/espresso"

RUN chown -R root:tagada /opt
RUN chmod -R g+w /opt

###############
## User land ##
###############
USER tagada
WORKDIR /home/tagada

# Installing Rust
RUN curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | bash -s -- -y
ENV PATH="/home/app/.cargo/bin:${PATH}"

# Installing SDKMan
RUN curl -s "https://get.sdkman.io" | bash -s

# Installing Java, Gradle and Kotlin
ARG JAVA_VERSION=17.0.11-tem
ARG GRADLE_VERSION=7.2
ARG KOTLIN_VERSION=1.9.23

RUN source "$HOME/.sdkman/bin/sdkman-init.sh" \
    && sdk install java ${JAVA_VERSION} \
    && sdk install gradle ${GRADLE_VERSION} \
    && sdk install kotlin ${KOTLIN_VERSION}

ENV PATH="${HOME}/.sdkman/candidates/java/current/bin:${PATH}"
ENV PATH="${HOME}/.sdkman/candidates/kotlin/current/bin:${PATH}"
ENV PATH="${HOME}/.sdkman/candidates/gradle/current/bin:${PATH}"

# Installing Tagada-Specs
RUN git clone https://gitlab.com/tagada-framework/tagada-specs.git specs
ENV BUNDLE_GEMFILE=/home/tagada/specs/Gemfile
RUN bundle install

# Installing Tagada-Tools
RUN git clone https://gitlab.com/tagada-framework/tagada-tools.git tools
WORKDIR /home/tagada/tools
RUN source /home/tagada/.profile \
    && cargo build --release

ENV PATH="/home/tagada/tools/target/release:${PATH}"

# Installing Tagada-Step2
WORKDIR /home/tagada
RUN git clone https://gitlab.com/tagada-framework/tagada-step2.git step2
WORKDIR /home/tagada/step2
RUN source "${HOME}/.sdkman/bin/sdkman-init.sh" \
    && gradle shadowJar

ENV STEP2=/home/tagada/step2/build/libs/tagada-step2-1.0.0-all.jar

USER tagada
WORKDIR /home/tagada
RUN echo 'alias tagada-specs="bundle exec"' >> .bashrc