# Test cipher specifications

Tagada-tools provides a means of checking that the specification graphs are correct with respect to the test vectors supplied by the authors of the ciphers.

This verification is provided by the command [`tagada-tools test vectors`](../reference/cli-reference/tagada-tools/test/vectors.md).

## Example AES-128

We want to try to see if the AES specification graph is correct. To do so we compute the aes-128 specification graph with the following command : 

```sh
tagada-specs specs/ciphers/rijndael.rb --plainsize 128 --keysize 128 > shared/aes128.spec.json
```

We take the test vectors from the [AES specifications](https://csrc.nist.gov/files/pubs/fips/197/final/docs/fips-197.pdf) :

```plain
block length 128 key length 128
Input = 32 43 f6 a8 88 5a 30 8d 31 31 98 a2 e0 37 07 34
Cipher key = 2b 7e 15 16 28 ae d2 a6 ab f7 15 88 09 cf 4f 3c
```

We can check that the graph specification is correct with :

```sh
tagada-tools test vectors \
  shared/aes128.spec.json \
  --base hex \
  --nb-encryption 1 \
  --key-values \
  2b,7e,15,16,28,ae,d2,a6,ab,f7,15,88,09,cf,4f,3c \
  --plaintext-values \
  32,43,f6,a8,88,5a,30,8d,31,31,98,a2,e0,37,07,34 \
  --expected-ciphertext \
  39,25,84,1d,02,dc,09,fb,dc,11,85,97,19,6a,0b,32
```

This will print the follwing output :

```sh
Encryption 1 ================================
__CIPHERTEXT__ [39, 25, 84, 1D, 2, DC, 9, FB, DC, 11, 85, 97, 19, 6A, B, 32]
__KEY__ [2B, 7E, 15, 16, 28, AE, D2, A6, AB, F7, 15, 88, 9, CF, 4F, 3C]
__PLAINTEXT__ [32, 43, F6, A8, 88, 5A, 30, 8D, 31, 31, 98, A2, E0, 37, 7, 34]
OK
```

