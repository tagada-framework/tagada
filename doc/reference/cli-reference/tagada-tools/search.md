# tagada-tools search

**Description** search cryptanalysis primitives

**Usage** `tagada-tools search`

```sh
Usage: tagada-tools search <COMMAND>

Commands:
  best-differential-characteristic
          Search for the differential characteristic with the highest probability.
  best-truncated-differential-characteristic
          Search for the truncated differential characteristic with the highest probability.
  help
          Print this message or the help of the given subcommand(s)

Options:
  -h, --help  Print help
```

## Description

search for some cryptanalysis primitives

## Subcommands

| **Command**                                                  | **Description**                                        |
| ------------------------------------------------------------ | ------------------------------------------------------ |
| [`tagada-tools search best-differential-characteristic`](transform/single-key.md) | compute the best differential characteristic           |
| [`tagada-tools search best-truncated-differential-characteristic`](transform/derive.md) | compute the best truncated differential characteristic |
| `tagada-tools search help`                                   | display the help                                       |

