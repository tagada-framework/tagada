# tagada-tools test vectors

**Description** test vectors against a Tagada specification graph

**Usage** `tagada-tools test vectors`

```sh
Usage: tagada-tools test vectors [OPTIONS] --key-values <KEY_VALUES> --plaintext-values <PLAINTEXT_VALUES> --base <BASE> [JSON_PATH]

Arguments:
  [JSON_PATH]  

Options:
  -k, --key-values <KEY_VALUES>                    
  -p, --plaintext-values <PLAINTEXT_VALUES>        
  -b, --base <BASE>                                [possible values: 2, 8, 10, 16]
  -t, --trace                                      
  -n, --nb-encryption <NB_ENCRYPTION>              [default: 1]
  -e, --expected-ciphertext <EXPECTED_CIPHERTEXT>  
  -h, --help                                       Print help
```

## Description

perform test vectors against a Tagada specification graph

## Options

| **Option**                  | **Default** | **Description**                                              |
| --------------------------- | ----------- | ------------------------------------------------------------ |
| `-k, --key-values`          |             | the key values, comma separated (**required**)               |
| `-p, --plaintext-values`    |             | the plaintext values, comma separated (**required**)         |
| `-b, --base`                |             | the encoding base of the provided values, may be either : 2 (binary), 8 (octal), 10 (decimal) or 16 (hexadecimal) |
| `-t,--trace`                |             | display the trace of the cipher (used generally for debugging) |
| `-n, --nb-encryption`       | `1`         | the number of encryption to performs                         |
| `-e, --expected-ciphertext` |             | the expected ciphertext                                      |
| `tagada-tools search help`  |             | display the help                                             |

