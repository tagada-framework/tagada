# tagada-tools search best-differential-characteristic picat

**Description** search the best differential characteristic using picat as step 1 backend solver

**Usage** `tagada-tools search best-differential-characteristic picat` 

```sh
Usage: tagada-tools search best-differential-characteristic <DIFF_JSON_PATH> <TRUNCATED_JSON_PATH> picat [OPTIONS]

Options:
  -p, --picat-path <PICAT_PATH>        
  -f, --fzn-picat-sat <FZN_PICAT_SAT>  
  -h, --help                           Print help
```

## Description

search the best differential characteristic using picat as step 1 backend solver.

## Options

| **Option**            | **Default**      | **Description**             |
| --------------------- | ---------------- | --------------------------- |
| `-p, --picat`         | `$PICAT`         | the `picat` path            |
| `-f, --fzn-picat-sat` | `$FZN_PICAT_SAT` | the `fzn_picat_sat.pi` path |
| `-h, --help`          |                  | display the help            |

