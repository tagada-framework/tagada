# tagada-tools search best-differential-characteristic

**Description** search the best differential characteristic

**Usage** `tagada-tools search best-differential-characteristic` 

```sh
	Usage: tagada-tools search best-differential-characteristic [OPTIONS] <DIFF_JSON_PATH> <TRUNCATED_JSON_PATH> <COMMAND>

Commands:
  picat     
  or-tools  
  gurobi    
  help      Print this message or the help of the given subcommand(s)

Arguments:
  <DIFF_JSON_PATH>       
  <TRUNCATED_JSON_PATH>  

Options:
  -m, --minizinc-path <MINIZINC_PATH>  
  -e, --encoding <ENCODING>            [default: cnf] [possible values: sum, dnf, cnf, table]
  -j, --jdk <JDK>                      
  -t, --tagada-step2 <TAGADA_STEP2>    
  -h, --help                           Print help
```

## Description

search for best differential characteristic. This search use a two-step solving process. In the first step, the solve look for best truncated differential characteristic. In the second step, the solve try to instanciate the truncated differential characteristic into a differential characteristic.

## Options

| **Option**            | **Default** | **Description**                                              |
| --------------------- | ----------- | ------------------------------------------------------------ |
| `-m, --minizinc-path` | `$MINIZINC` | the `minizinc`path                                           |
| `-e, --encoding`      | `cnf`       | the type of constraint used to represent truth tables. this can be conjunctive normal form (`cnf`), disjunctive normal form (`dnf`), constraint table (`table`) or sums (`sum`) |
| `-j, --jdk`           |             | the `jdk` path used to execute Tagada Step2                  |
| `-t, --tagada-step2`  | `$STEP2`    | the `tagada-step2` jar path.                                 |
| `-h, --help`          |             | display the help                                             |

## Subcommands

| **Command**                                                  | **Description**                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [`tagada-tools search best-differential-characteristic picat`](transform/single-key.md) | use picat as backend solver to search truncated differentials |
| [`tagada-tools search best-differential-characteristic or-tools`](transform/derive.md) | use or-tools as backend solver to search truncated differentials |
| [`tagada-tools search best-differential-characteristic gurobi`](transform/derive.md) | use gurobi as backend solver to search truncated differentials |
| `tagada-tools search best-differential-characteristic help`  | display the help                                             |

