# tagada-tools transform single-key

**Description** generate a new specification graph without the keyschedule

**Usage** `tagada-tools transform single-key`

```sh
Usage: tagada-tools transform single-key [JSON_PATH]

Arguments:
  [JSON_PATH]  

Options:
  -h, --help  Print help
```

## Description

generate a new specification graph without the keyschedule. If no path is provided, the graph is readed from `stdin`.

## Options

| **Option**   | Default | **Description**  |
| ------------ | ------- | ---------------- |
| `-h, --help` |         | display the help |

