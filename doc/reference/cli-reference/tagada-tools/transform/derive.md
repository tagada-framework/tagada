# tagada-tools transform derive

**Description** generate differential graph from a specification graph

**Usage** `tagada-tools transform derive`

```sh
Usage: tagada-tools transform derive [JSON_PATH]

Arguments:
  [JSON_PATH]  

Options:
  -h, --help  Print help
```

## Description

generate differential graph from a specification graph. If no path is provided, the graph is readed from `stdin`.

## Options

| **Option**   | Default | **Description**  |
| ------------ | ------- | ---------------- |
| `-h, --help` |         | display the help |

