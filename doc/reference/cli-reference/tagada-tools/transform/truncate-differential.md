# tagada-tools transform truncate-differential

**Description** generate a truncated differential graph from a differential graph

**Usage** `tagada-tools transform truncate-differential`

```sh
Usage: tagada-tools transform truncate-differential [OPTIONS] [JSON_PATH]

Arguments:
  [JSON_PATH]  

Options:
  -g, --generate-xors <GENERATE_XORS>  [default: 0]
  -d, --diff-variables                 
  -t, --transitivity                   
  -h, --help                           Print help
```

## Description

generate differential graph from a specification graph. If no path is provided, the graph is readed from `stdin`.

## Options

| **Option**             | Default | **Description**                                              |
| ---------------------- | ------- | ------------------------------------------------------------ |
| `-g, --generate-xors`  | `0`     | generate redondant xor equations in order to tighten the abstraction. the provided parameter is the maximal size of the equations. The larger the size, the greater the abstraction, but generation takes longer and the model is slower. |
| `-d, --diff-variables` |         | generate diff variables. Diff variables are created in order to ensure that there is no incompatibility between pair of xor equations. |
| `-t, --transitivity`   |         | ensure that if $`a = b \land b = c \implies a = c`$          |
| `-h, --help`           |         | display the help                                             |

