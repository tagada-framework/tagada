# tagada-tools test

**Description** perform some tests

**Usage** `tagada-tools test`

```sh
Usage: tagada-tools test <COMMAND>

Commands:
  vectors  
  help     Print this message or the help of the given subcommand(s)

Options:
  -h, --help  Print help
```

## Description

perform some tests

## Subcommands

| **Command**                                    | **Description**                                   |
| ---------------------------------------------- | ------------------------------------------------- |
| [`tagada-tools test vectors`](test/vectors.md) | test vectors against a Tagada specification graph |
| `tagada-tools search help`                     | display the help                                  |

