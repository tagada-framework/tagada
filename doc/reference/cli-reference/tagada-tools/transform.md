# tagada-tools transform

**Description** transform a Tagada graph

**Usage** `tagada-tools transform`

```sh
Usage: tagada-tools transform <COMMAND>

Commands:
  single-key             Transform a specification graph into another specification graph without the keyschedule.
  derive                 Transform a specification graph into a differential graph.
  truncate-differential  Transform a differential graph into a truncated differential graph.
  help                   Print this message or the help of the given subcommand(s)

Options:
  -h, --help  Print help
```

## Description

Transform a Tagada graph into another one.

## Subcommands

| **Command**                                                  | **Description**                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [`tagada-tools transform single-key`](transform/single-key.md) | remove the single key from a specification graph. this function is used to create single-key differential attacks. |
| [`tagada-tools transform derive`](transform/derive.md)       | transform a specification graph into a differential graph    |
| [`tagada-tools transform truncate-differential`](transform/truncate-differential.md) | transform a differential graph into a truncated differential graph |
| `tagada-tools transform help`                                | display the help                                             |

