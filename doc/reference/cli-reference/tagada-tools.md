# tagada-tools

**Description** use tagada-tools

**Usage** `tagada-tools`

```sh
Usage: tagada-tools <COMMAND>

Commands:
  transform          
  generate-graphviz  
  evaluate           
  test               
  search             
  help               Print this message or the help of the given subcommand(s)

Options:
  -h, --help  Print help
```

## Description

provides most of Tagada's functionality

## Subcommands

| **Command**                                                  | **Description**                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [`tagada-tools transform`](tagada-tools/transform.md)        | apply a given transformation to a tagada file, _e.g._ transform a specification graph into a differential graph |
| [`tagada-tools generate-graphviz`](tagada-tools/generate-graphviz.md) | generate a graphviz `.dot`file from a tagada graph           |
| [`tagada-tools evaluate`](tagada-tools/evaluate.md)          | evaluate a Tagada graph with given parameters, _e.g_ evaluate a differetential characteristic |
| [`tagada-tools test`](tagada-tools/test.md)                  | perform tests on a Tagada graph                              |
| [`tagada-tools search`](tagada-tools/search.md)              | search cryptanalysis primitive using some backend solvers    |
| `tagada-tools help`                                          | display the help                                             |



