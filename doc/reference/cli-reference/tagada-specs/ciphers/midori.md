

# bundle exec specs/ciphers/midori

**Description** generate a specification graph of `-r` rounds of the Midori cipher.

**Usage** `bundle exec specs/ciphers/midori.rb`

**Aliases** `tagada-specs specs/ciphers/midori.rb`

```sh
Usage: bundle exec specs/ciphers/midori.rb [options]

Options:
    -v, --version  key size ({64, 128})
    -r, --round    number of rounds
    --help         print the help
```

## Description

generate a specification graph of `-r` rounds of the Midori cipher.

## Options

| **Option**      | **Default** | **Description**                                              |
| --------------- | ----------- | ------------------------------------------------------------ |
| `-v, --version` |             | key size either 64 or 128 bits (**required**)                |
| `-r, --round`   | `null`      | the number of rounds to generate. If no value is provided, the script will generate the default number of round for the `--version` parameter |
| `-h, --help`    |             | display the help                                             |



