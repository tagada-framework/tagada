

# bundle exec specs/ciphers/heys.rb

**Description** generate a specification graph of `-r` rounds of the Heys cipher.

**Usage** `bundle exec specs/ciphers/heys.rb`

**Aliases** `tagada-specs specs/ciphers/heys.rb`

```sh
Usage: bundle exec specs/ciphers/heys.rb [options]

Options:
    -r, --round  number of rounds
    --help       print the help
```

## Description

generate a specification graph of `-r` rounds of the Heys cipher (toy example).

## Options

| **Option**    | **Default** | **Description**                 |
| ------------- | ----------- | ------------------------------- |
| `-r, --round` | `5`         | the number of round to generate |
| `-h, --help`  |             | display the help                |



