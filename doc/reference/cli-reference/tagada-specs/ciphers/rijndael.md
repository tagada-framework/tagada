

# bundle exec specs/ciphers/rijndael.rb

**Description** generate a specification graph of `-r` rounds of the Rijndael cipher.

**Usage** `bundle exec specs/ciphers/rijndael.rb`

**Aliases** `tagada-specs specs/ciphers/rijndael.rb`

```sh
Usage: bundle exec specs/ciphers/rijndael.rb [options]

Options:
    -k, --keysize    key size ({128,160,192,224,256})
    -p, --plainsize  plaintext size ({128,160,192,224,256})
    -r, --round      number of rounds
    --help           print the help
```

## Description

generate a specification graph of `-r` rounds of the Rijndael cipher.

## Options

| **Option**        | **Default** | **Description**                                              |
| ----------------- | ----------- | ------------------------------------------------------------ |
| `-k, --keysize`   |             | the used key size (**required**)                             |
| `-p, --plainsize` |             | the used plaintext size (**required**)                       |
| `-r, --round`     | `null`      | the number of rounds to generate. If no value is provided, the script will generate the default number of round for the `--keysize` and `--plainsize` parameters |
| `-h, --help`      |             | display the help                                             |



