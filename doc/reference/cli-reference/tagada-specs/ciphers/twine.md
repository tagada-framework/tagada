

# bundle exec specs/ciphers/twine.rb

**Description** generate a specification graph of `-r` rounds of the Twine cipher.

**Usage** `bundle exec specs/ciphers/twine.rb`

**Aliases** `tagada-specs specs/ciphers/twine.rb`

```sh
Usage: bundle exec specs/ciphers/twine.rb [options]

Options:
    -v, --version  key size ({80, 128})
    -r, --round    number of rounds
    --help         print the help
```

## Description

generate a specification graph of `-r` rounds of the Twine cipher.

## Options

| **Option**      | **Default** | **Description**                                              |
| --------------- | ----------- | ------------------------------------------------------------ |
| `-v, --version` |             | the cipher version either 80 or 128 bits (**required**)      |
| `-r, --round`   | `null`      | the number of rounds to generate. If no value is provided, the script will generate the default number of round for the `--version`parameter |
| `-h, --help`    |             | display the help                                             |



