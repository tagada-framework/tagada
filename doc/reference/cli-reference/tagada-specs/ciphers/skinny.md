

# bundle exec specs/ciphers/skinny.rb

**Description** generate a specification graph of `-r` rounds of the Skinny cipher.

**Usage** `bundle exec specs/ciphers/skinny.rb`

**Aliases** `tagada-specs specs/ciphers/skinny.rb`

```sh
Usage: specs/ciphers/skinny.rb [options]

Options:
    -v, --version     version {64, 128}
    -r, --round       number of rounds {1,2,...}
    -tk, --tweak-key  Skinny inputs configuration {0,1,2,3}
    --help            print the help
```

## Description

generate a specification graph of `-r` rounds of the Skinny cipher.

## Options

| **Option**         | **Default** | **Description**                                              |
| ------------------ | ----------- | ------------------------------------------------------------ |
| `-v, --version`    |             | the cipher version either 64 or 128 bits (**required**)      |
| `-tk, --tweak-key` |             | the tweakey size 0, 1, 2 or 3 (**required**)                 |
| `-r, --round`      | `null`      | the number of rounds to generate. If no value is provided, the script will generate the default number of round for the `--version` and `--tweak-key` parameters |
| `-h, --help`       |             | display the help                                             |



