

# bundle exec specs/ciphers/warp.rb

**Description** generate a specification graph of `-r` rounds of the Warp cipher.

**Usage** `bundle exec specs/ciphers/warp.rb`

**Aliases** `tagada-specs specs/ciphers/warp.rb`

```sh
Usage: specs/ciphers/warp.rb [options]

Options:
    -r, --round  number of rounds
    --help       print the help
```

## Description

generate a specification graph of `-r` rounds of the Warp cipher.

## Options

| **Option**    | **Default** | **Description**                                              |
| ------------- | ----------- | ------------------------------------------------------------ |
| `-r, --round` | `null`      | the number of rounds to generate. If no value is provided, the script will generate the default number of round. |
| `-h, --help`  |             | display the help                                             |
