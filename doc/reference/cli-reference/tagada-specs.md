# tagada-specs

**Description** generate tagada specification graphs

**Usage** `bundle exec`

**Aliases** `tagada-specs`

```sh
Usage: bundle exec <CIPHER>

Cipher:
  specs/ciphers/heys.rb          
  specs/ciphers/midori.rb
  specs/ciphers/rijndael.rb
  specs/ciphers/skinny.rb
  specs/ciphers/twine.rb
  specs/ciphers/warp.rb
```

## Description

generate Tagada specification graphs

## Subcommands

| **Command**                                                  | **Description**                                              |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [`bundle exec specs/ciphers/heys.rb`](tagada-specs/ciphers/heys.md) | generate a specification graph for the Heys cipher (toy example) |
| [`bundle exec specs/ciphers/midori.rb`](tagada-specs/ciphers/midori.md) | generate a specification graph for Midori                    |
| [`bundle exec specs/ciphers/rijndael.rb`](tagada-specs/ciphers/rijndael.md) | generate a specification graph for Rijndael                  |
| [`bundle exec specs/ciphers/skinny.rb`](tagada-specs/ciphers/skinny.md) | generate a specification graph for Skinny                    |
| [`bundle exec specs/ciphers/twine.rb`](tagada-specs/ciphers/twine.md) | generate a specification graph for Twine                     |
| [`bundle exec specs/ciphers/warp.rb`](tagada-specs/ciphers/warp.md) | generate a specification graph for Warp                      |



