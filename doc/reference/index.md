# Reference documentation

## File formats

[Tagada Specification Graph](file-formats/specification-graph.md)

[Tagada Differential Graph](file-formats/differential-graph.md)

[Tagada Truncated Differential Graph](file-formats/truncated-differential-graph.md)

## Command-line interfaces (CLIs)

[tagada-specs](cli-reference/tagada-specs.md)

[tagada-tools](cli-reference/tagada-tools.md)

[tagada-step2](.)