# Get Tagada

For now the easiest way to get Tagada is to use the provided [Dockerfile](../../Dockerfile). To use it, you must have [Docker](https://docs.docker.com/get-docker/) installed.

```sh
docker build -t tagada "https://gitlab.com/tagada-framework/tagada/-/raw/main/Dockerfile?ref_type=heads"
```

Once the docker image is created you can either run the tagada scripts directly or enter in the docker image.

## Enter in the docker container

To enter in a Tagada container you can use the following command.

```sh
docker run -it --platform linux/amd64 --mount type=bind,source="$(pwd)"/shared,target=/home/tagada/shared tagada
```

The option `--platform linux/amd64` is required for ARM or Apple Sillicon users as the AMR Dockerfile is not release yet.

The `--mount type=bind,source="$(pwd)"/shared,target=/home/tagada/shared` option allows to share the current directory between the host and the docker container. This is required to save the computation results.

### Computing a best differential characteristic

In order to compute the best differential characteristic for a given cipher, you should follow 5 steps. For the following tutorial, we will compute a related-key differential characteristic for the heys cipher.

1. Enter in the Tagada container by using the previous command.

2. Generate the specification graph for the Heys cipher.

   ```sh
   tagada-specs specs/ciphers/heys.rb > shared/heys.spec.json
   ```

   This command will generate the keys specification graph into heys.spec.json. You can register the file where you want, but if you want to save the file outside the container you should use a mounted directory, such that the shared folder.

3. Transform the specification graph into a differential one.

   In order to compute a best differential characteristic, you should transform the specification graph into a differential one. This step transform the S-Boxes functions into DDT relations.

   ```sh
   tagada-tools transform derive shared/heys.spec.json > shared/heys.diff.json
   ```

4. Once the differential graph is computed, you should compute the truncated differential graph. Indeed, Tagada computes the best differential characteristic by following a 2-step computation. In the first step, Tagada computes truncated differential characteristics and in the second step, Tadaga instantiates the truncated differential characteristics into differential characteristics.

   ```sh
   tagada-tools transform truncate-differential shared/heys.diff.json > shared/heys.trun.json
   ```

5. Here you can compute the best differential characteristic of the cipher by using :

   ```sh
   tagada-tools search best-differential-characteristic shared/heys.diff.json shared/heys.trun.json picat
   ```

​	This will print :

```json
{"u^3__3":0,"u^2__0":0,"v^3__0":0,"S^2_3":0,"__CIPHERTEXT____3":0,"w^2__1":0,"out_14":0,"K^4__1":0,"__K_1__":0,"__KEY____5":0,"obj":0,"u^2__3":0,"in_1":0,"K^5__2":0,"__KEY____1":0,"v^3__2":0,"K^1__3":0,"y__1":0,"__CIPHERTEXT____0":0,"in_10":0,"in_11":0,"w^0__2":0,"K^3__3":0,"u^3__0":0,"K^4__2":0,"w^2__3":0,"out_5":0,"S^1_3":0,"out_2":0,"u^3__1":0,"in_5":0,"out_3":0,"out_11":0,"w^3__1":0,"u^1__3":0,"K^2__2":0,"S^1_2":0,"u^4__2":0,"v^4__3":0,"u^3__2":0,"in_8":0,"w^3__0":0,"w^1__1":0,"v^2__2":0,"__PLAINTEXT____0":1,"__KEY____3":0,"out_10":0,"w^3__2":0,"x__3":0,"out_9":0,"u^4__0":0,"S^1_1":0,"w^1__2":0,"u^1__0":0,"u^2__1":0,"__P_1__":0,"__KEY____2":0,"K^3__2":0,"K^2__1":0,"in_2":0,"v^1__1":0,"__K_0__":1,"__PLAINTEXT____1":0,"__C_3__":0,"S^3_1":0,"__CIPHERTEXT____2":0,"K^4__3":0,"v^3__3":0,"K^5__0":0,"v^2__0":0,"K^5__3":0,"v^2__3":0,"K^1__1":0,"S^2_1":0,"__K_3__":0,"w^2__2":0,"out_12":0,"__CIPHERTEXT____1":0,"__P_2__":0,"u^2__2":0,"x__0":1,"__KEY____6":0,"in_4":0,"K^2__0":0,"__C_1__":0,"out_13":0,"w^3__3":0,"S^3_3":0,"__K_5__":0,"__KEY____0":1,"x__2":0,"__C_2__":0,"__PLAINTEXT____3":0,"in_14":0,"__K_6__":0,"__P_3__":0,"K^3__0":0,"K^3__1":0,"v^4__0":0,"w^1__3":0,"in_9":0,"w^1__0":0,"__PLAINTEXT____2":0,"S^2_2":0,"x__1":0,"out_6":0,"S^1_0":0,"S^3_0":0,"S^2_0":0,"w^0__1":0,"in_7":0,"u^4__1":0,"w^0__0":1,"K^4__0":0,"v^3__1":0,"out_4":0,"K^1__0":1,"__C_0__":0,"in_12":0,"K^5__1":0,"K^1__2":0,"in_15":0,"out_8":0,"S^3_2":0,"out_15":0,"v^4__1":0,"y__3":0,"__K_4__":0,"__KEY____7":0,"u^4__3":0,"u^1__2":0,"__P_0__":1,"in_3":0,"out_0":0,"out_1":0,"K^2__3":0,"__KEY____4":0,"v^1__2":0,"v^1__0":0,"v^1__3":0,"w^0__3":0,"u^1__1":0,"__K_7__":0,"v^4__2":0,"y__0":0,"out_7":0,"__K_2__":0,"v^2__1":0,"y__2":0,"in_6":0,"w^2__0":0,"in_0":0,"in_13":0}
```

The probability of the differential characteristic is given by the `obj` variable. The result is represented as a $`-log_2`$, _e. g._ :

$$
\mathtt{obj = 0} \Leftrightarrow p = 2^{0} \\
\mathtt{obj = 3100} \Leftrightarrow p = 2^{-31.00}
$$
