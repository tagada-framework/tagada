# Manuals

This section contains user guides on how to install, set up, configure, and use Tagada tools.

To get Tagada please go [here](get-tagada.md).

## Modules

### [Tagada-Specs]()

Tagada-specs is the module responsible for generating graphs from the encryption specification.

### [Tagada-Tools]()

Tagada-tools provide the majority of Tagada's tools.

### [Tagada-Step2]()

Tagada-step2 two is a library to instantiate truncated differential characteristics into differential characteristics.