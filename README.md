# Tagada

TAGADA: a Tool for Automatic Generation of Abstraction-based Differential Attacks

## Get Tagada

Learn how to install Tagada on MAC or Linux.

### Guides

[Get started with Tagada and learn how Tagada can compute automatically cryptanalysis primitives.](doc/guides/index.md)

### Manuals

[Learn how to install, set up, configure and use Tagada.](doc/manuals/index.md)

### Reference

[Browse CLI documentation.](doc/reference/index.md)

