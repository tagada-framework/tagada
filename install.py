#!/usr/bin/python3

import argparse
import os
import platform
import shutil

import yaml

import shell
from shell import Bin, cd
from utils import eprint


class UnsupportedConfigurationError(Exception):
    """Raise when the configuration is not supported"""


def get_pkg_manager():
    uname = os.uname()
    _pkg_manager = None
    if uname.sysname == "Linux":
        pkg_managers = {
            'fedora': 'dnf',
            'ubuntu': 'apt',
            'debian': 'apt'
        }
        distribution = platform.freedesktop_os_release()["ID"]
        if distribution not in pkg_managers:
            raise UnsupportedConfigurationError(
                f"the distribution {distribution} is not supported. the list of supported distributions is: {', '.join(pkg_managers.keys())}")
        _pkg_manager = Bin(pkg_managers[distribution])
    else:
        raise UnsupportedConfigurationError(f"{uname.sysname} is not supported")
    return _pkg_manager


# Utilities
python3 = Bin("python3")

git = Bin("git")
git.clone = git % "clone"
git.checkout = git % "checkout"

sudo = Bin("sudo")

pkg_manager = get_pkg_manager()
pkg_manager.install = pkg_manager % "install"


def install_git():
    if shutil.which("git") is None:
        eprint("Installing git")
        (sudo % pkg_manager.install)("git")


def generate_env(config):
    env_sh = [
        "#!/usr/bin/bash",
        "# Home",
        'export TAGADA_HOME=$(realpath "$PWD")'
    ]

    if "modules" in config:
        for module in config["modules"]:
            source_file = f"{module}/env.sh"
            env_sh.append(f'[[ -f "{source_file}" ]] && source "{source_file}"')

    with open("env.sh", "w") as env_sh_file:
        env_sh_file.write("\n".join(env_sh))


if __name__ == '__main__':
    parser = argparse.ArgumentParser("install.py")
    parser.add_argument("-v", "--verbose", help="enable_verbosity", action="store_true")
    parser.add_argument("--https", help="use https instead of ssh", action="store_true")
    args = parser.parse_args()

    if args.verbose:
        shell.VERBOSITY = True

    base_url = "git@gitlab.com:tagada-framework/"
    if args.https:
        base_url = "https://gitlab.com/tagada-framework/"

    install_git()
    with open("config.yml", "r") as f:
        config = yaml.safe_load(f)
    if "modules" in config:
        for (module, version) in config["modules"].items():
            git.clone(base_url + f"{module}.git")
            with cd(module):
                git.checkout(version)
                if os.path.exists("install.py"):
                    python3("install.py")
    generate_env(config)
